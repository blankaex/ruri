#!/usr/bin/env sh
set -e

dir_src="./ruri/"
dir_aur="./aur/"
dir_package_pip="./package_pip/"
dir_package_aur="./package_aur/"

rm -rf ./package_*/
mkdir -p ${dir_package_pip}
cd ${dir_package_pip}
cp ../*.* .
cp -R ../${dir_src} .
python3 setup.py sdist bdist_wheel
twine upload --repository-url https://upload.pypi.org/legacy/ dist/*
cd ..
rm -rf ${dir_package_pip}
read -srn 1 -p "Please wait until Pypi has processed the upload, then press any key..."
mkdir -p ${dir_package_aur}
cd ${dir_package_aur}
cp ../${dir_aur}/PKGBUILD .
makepkg --printsrcinfo > ../${dir_aur}/.SRCINFO
makepkg -Acsf
cd ..
rm -rf ${dir_package_aur}
printf "Done!\n"
exit 0

